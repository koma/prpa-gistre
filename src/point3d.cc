#include "point3d.hh"

#include <cmath>

Point3D::Point3D(double x_, double y_, double z_)
    : x(x_)
    , y(y_)
    , z(z_)
{}

Point3D::~Point3D()
{}

double  Point3D::getX() const { return x; }

double  Point3D::getY() const { return y; }

double  Point3D::getZ() const { return z; }

void    Point3D::setX(double x_) { x = x_; }

void    Point3D::setY(double y_) { x = y_; }

void    Point3D::setZ(double z_) { x = z_; }

double  Point3D::distance(Point3D point) const
{
    double dx = point.x - x;
    double dy = point.y - y;
    double dz = point.z - z;

    return sqrt(dx * dx + dy * dy + dz * dz);
}
