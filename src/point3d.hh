#ifndef POINT3D_HH_
# define POINT3D_HH_

class Point3D
{
public:
    explicit Point3D(double x_, double y_, double z_);
    ~Point3D();

    double  getX() const;
    double  getY() const;
    double  getZ() const;

    void    setX(double x);
    void    setY(double y);
    void    setZ(double z);

    // Euclienne distance
    double distance(Point3D point) const;

private:
    double x;
    double y;
    double z;
};

#endif /* !POINT3D_HH_ */
