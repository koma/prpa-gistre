#ifndef TIMER_HH_
# define TIMER_HH

# include <chrono>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::nanoseconds;
using std::chrono::steady_clock;

struct Timer
{
public:
    Timer(double& s)
        : seconds(s)
        , t0(steady_clock::now())
    {}

    ~Timer()
    {
        duration<double> d =
            duration_cast<duration<double> >(steady_clock::now() - t0);
        seconds = d.count();
    }

    double&                       seconds;
    steady_clock::time_point      t0;
};

#endif /* !TIMER_HH_ */
