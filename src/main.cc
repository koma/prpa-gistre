#include <iostream>
#include <string>
#include <SDL/SDL.h>

#include "defines.hh"
#include "timer.hh"

static void usage(char* prog)
{
    std::cout << "usage: " << prog
        << " [--tbb] n_iterations filename" << std::endl
        << "\t--tbb : enable parallel mode" << std::endl;
}

static int check_args(int argc, char** argv,
                      int& iterations, std::string& filename)
{
    int mode = SEQ_MODE;

    if (argc == 4)
    {
        std::string mode(argv[1]);
        if (mode != "--tbb")
        {
            usage(argv[0]);
            return -1;
        }
        mode = TBB_MODE;
        iterations = std::stoi(argv[2]);
        filename = std::string(argv[3]);
    }
    else
    {
        iterations = std::stoi(argv[1]);
        filename = std::string(argv[2]);
    }

    return mode;
}

int main(int argc, char** argv)
{
    if (argc < 3 || argc > 4)
    {
        usage(argv[0]);
        exit(1);
    }

    double      ellapsed_time = 0;
    int         mode = SEQ_MODE;
    int         iterations = 0;
    std::string filename;

    if ((mode = check_args(argc, argv, iterations, filename)) == -1)
    {
        usage(argv[0]);
        exit(1);
    }

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
        std::cerr << "Erreur d'initialisation de la SDL" << std::endl;
        exit(EXIT_FAILURE);
    }

    Timer t(ellapsed_time);

    std::cout << "Ellapsed time: " << ellapsed_time << " ms" << std::endl;

    SDL_Quit();
    return EXIT_SUCCESS;
}
