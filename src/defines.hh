#ifndef DEFINES_HH_
# define DEFINES_HH_

// Sequential mode
# define SEQ_MODE 0

// Parallel mode
# define TBB_MODE 1

#endif /* !DEFINES_HH_ */
