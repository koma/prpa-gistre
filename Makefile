# Makefile

BIN = prpa

all:
	make -C src/

clean::
	rm -f *~ *core *.a *.so *.log
	make -C src/ clean

distclean:: clean
	make -C src/ distclean
	rm -f $(BIN)

# EOF
